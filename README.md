


def unique_lists(A):
    tupels = set([tuple(sorted(x)) for x in A])
    print(len(tupels))
    unique_list = [list(x) for x in tupels]
    return unique_list

res = unique_lists([[1, 2], [2, 1], [1], [1, 1, 1], [1,2], [], [] , [0]])

print(res)


#!/bin/python3

import math
import os
import random
import re
import sys

def openPair(a):
    if a=='}':
        return '{'
    if a==']':
        return '['
    if a==')':
        return '('
    

# Complete the isBalanced function below.
def isBalanced(s):
    brackets_list = []
    open_brackets = "[{("
    close_brackets = "]})"
    
    for x in s:
        if x in open_brackets:
            brackets_list.append(x)
        elif x in close_brackets:
            if len(brackets_list) == 0:
                return "NO"
            last_opend = brackets_list.pop()
            if last_opend != openPair(x):
                return "NO"
    print(brackets_list)
    if len(brackets_list) == 0:
        return "YES"
    else:
        return "NO"
    
    
    
if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    t = int(input())

    for t_itr in range(t):
        s = input()

        result = isBalanced(s)

        fptr.write(result + '\n')

    fptr.close()



